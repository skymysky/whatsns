<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Tags extends CI_Controller {
	function __construct() {
		$this->whitelist = "index,view,all,question,article";
		parent::__construct ();
		$this->load->model ( "tag_model" );
	}
	function index() {
		$navtitle = '标签列表';
		$metakeywords = $navtitle;
		$metadescription = '标签列表';
		
		include template ( 'tag' );
	}
	
	/**
	 *
	 * 标签详情--标签动态
	 *
	 * @date: 2018年11月7日 下午2:11:41
	 *
	 * @author : 61703
	 *        
	 * @param
	 *        	: variable
	 *        	
	 * @return :
	 *
	 */
	function view() {
		$tagalias = htmlspecialchars ( $this->uri->segments [2] );
		
		// 通过标签别名查询标签
		$tag = $this->tag_model->get_by_tagalias ( $tagalias );
		if(!$tag){
		        $url=url("tags");
				header ( 'HTTP/1.1 404 Not Found' );
				header ( "status: 404 Not Found" );
				echo '<!DOCTYPE html><html><head><meta charset=utf-8 /><title>404-您访问的页面不存在</title>';
				echo "<style>body { background-color: #ECECEC; font-family: 'Open Sans', sans-serif;font-size: 14px; color: #3c3c3c;}";
				echo ".nullpage p:first-child {text-align: center; font-size: 150px;  font-weight: bold;  line-height: 100px; letter-spacing: 5px; color: #fff;}";
				echo ".nullpage p:not(:first-child) {text-align: center;color: #666;";
				echo "font-family: cursive;font-size: 20px;text-shadow: 0 1px 0 #fff;  letter-spacing: 1px;line-height: 2em;margin-top: -50px;}";
				echo ".nullpage p a{margin-left:10px;font-size:20px;}";
				echo '</style></head><body> <div class="nullpage"><p><span>4</span><span>0</span><span>4</span></p><p>标签已经被删除！⊂((δ⊥δ))⊃<a href="'.$url.'">返回标签页面</a></p></div></body></html>';
				exit ();
		
		}
		$page = max ( 1, intval ( $this->uri->segments [3] ) );
		$pagesize = $this->setting ['list_default'];
		$startindex = ($page - 1) * $pagesize;
		$rownum = returnarraynum ( $this->db->query ( getwheresql ( 'tag_item', " tagid=" . $tag ['id'], $this->db->dbprefix ) )->row_array () );
		;
		
		// 获取tag动态列表
		$tagdoinglist = $this->tag_model->getlistbytagid ( $tag ['id'], $startindex, $pagesize );
		
		if ($tagdoinglist) {
			// 获取相关标签
			$relativetags = $this->tag_model->gettaglistbycid ( $tagdoinglist [0] ['cid'] );
		}
		$this->setting ['seo_index_description'] && $_seo_description = str_replace ( "{wzmc}", $this->setting ['site_name'], $this->setting ['seo_index_description'] );
		$this->setting ['seo_index_keywords'] && $_seo_keywords = str_replace ( "{wzmc}", $this->setting ['site_name'], $this->setting ['seo_index_keywords'] );
		
		$departstr = page ( $rownum, $pagesize, $page, "tags/view/$tagalias" );
		$navtitle = $tag ['title'];
		$this->setting ['seo_index_description'] && $_seo_description = str_replace ( "{wzmc}", $this->setting ['site_name'], $this->setting ['seo_index_description'] );
		$this->setting ['seo_index_keywords'] && $_seo_keywords = str_replace ( "{wzmc}", $this->setting ['site_name'], $this->setting ['seo_index_keywords'] );
		
		$seo_description = $tag ['description']? $tag ['description']:$_seo_description;
		$seo_keywords = $tag ['keywords']? $tag ['keywords']:$_seo_keywords;
		include template ( 'tagview' );
	}
	/**
	 *
	 * 问答标签
	 *
	 * @date: 2018年11月7日 下午2:12:35
	 *
	 * @author : 61703
	 *        
	 * @param
	 *        	: variable
	 *        	
	 * @return :
	 *
	 */
	function question() {
		$tagalias = htmlspecialchars ( $this->uri->segments [3] );
		
		// 通过标签别名查询标签
		$tag = $this->tag_model->get_by_tagalias ( $tagalias );
		if(!$tag){
			$url=url("tags");
			header ( 'HTTP/1.1 404 Not Found' );
			header ( "status: 404 Not Found" );
			echo '<!DOCTYPE html><html><head><meta charset=utf-8 /><title>404-您访问的页面不存在</title>';
			echo "<style>body { background-color: #ECECEC; font-family: 'Open Sans', sans-serif;font-size: 14px; color: #3c3c3c;}";
			echo ".nullpage p:first-child {text-align: center; font-size: 150px;  font-weight: bold;  line-height: 100px; letter-spacing: 5px; color: #fff;}";
			echo ".nullpage p:not(:first-child) {text-align: center;color: #666;";
			echo "font-family: cursive;font-size: 20px;text-shadow: 0 1px 0 #fff;  letter-spacing: 1px;line-height: 2em;margin-top: -50px;}";
			echo ".nullpage p a{margin-left:10px;font-size:20px;}";
			echo '</style></head><body> <div class="nullpage"><p><span>4</span><span>0</span><span>4</span></p><p>标签已经被删除！⊂((δ⊥δ))⊃<a href="'.$url.'">返回标签页面</a></p></div></body></html>';
			exit ();
			
		}
		$page = max ( 1, intval ( $this->uri->segments [4] ) );
		$pagesize = $this->setting ['list_default'];
		$startindex = ($page - 1) * $pagesize;
		$rownum = returnarraynum ( $this->db->query ( getwheresql ( 'tag_item', " tagid=" . $tag ['id'] . " and itemtype='question'", $this->db->dbprefix ) )->row_array () );
		;
		
		// 获取tag动态列表
		$tagdoinglist = $this->tag_model->getlistbytagid ( $tag ['id'], $startindex, $pagesize, 'question' );
		if ($tagdoinglist) {
			// 获取相关标签
			$relativetags = $this->tag_model->gettaglistbycid ( $tagdoinglist [0] ['cid'] );
		}
		
		$departstr = page ( $rownum, $pagesize, $page, "tags/question/$tagalias" );
		$navtitle =  $tag ['tagname'].'相关问题列表-'.$tag ['title'];
		$this->setting ['seo_index_description'] && $_seo_description = str_replace ( "{wzmc}", $this->setting ['site_name'], $this->setting ['seo_index_description'] );
		$this->setting ['seo_index_keywords'] && $_seo_keywords = str_replace ( "{wzmc}", $this->setting ['site_name'], $this->setting ['seo_index_keywords'] );
		
		$seo_description = $tag ['description']? $tag ['description']:$_seo_description;
		$seo_keywords = $tag ['keywords']? $tag ['keywords']:$_seo_keywords;
		include template ( 'tagview' );
	}
	/**
	 *
	 * 文章标签
	 *
	 * @date: 2018年11月7日 下午2:18:59
	 *
	 * @author : 61703
	 *        
	 * @param
	 *        	: variable
	 *        	
	 * @return :
	 *
	 */
	function article() {
		$tagalias = htmlspecialchars ( $this->uri->segments [3] );
		
		// 通过标签别名查询标签
		$tag = $this->tag_model->get_by_tagalias ( $tagalias );
		if(!$tag){
			$url=url("tags");
			header ( 'HTTP/1.1 404 Not Found' );
			header ( "status: 404 Not Found" );
			echo '<!DOCTYPE html><html><head><meta charset=utf-8 /><title>404-您访问的页面不存在</title>';
			echo "<style>body { background-color: #ECECEC; font-family: 'Open Sans', sans-serif;font-size: 14px; color: #3c3c3c;}";
			echo ".nullpage p:first-child {text-align: center; font-size: 150px;  font-weight: bold;  line-height: 100px; letter-spacing: 5px; color: #fff;}";
			echo ".nullpage p:not(:first-child) {text-align: center;color: #666;";
			echo "font-family: cursive;font-size: 20px;text-shadow: 0 1px 0 #fff;  letter-spacing: 1px;line-height: 2em;margin-top: -50px;}";
			echo ".nullpage p a{margin-left:10px;font-size:20px;}";
			echo '</style></head><body> <div class="nullpage"><p><span>4</span><span>0</span><span>4</span></p><p>标签已经被删除！⊂((δ⊥δ))⊃<a href="'.$url.'">返回标签页面</a></p></div></body></html>';
			exit ();
			
		}
		$page = max ( 1, intval ( $this->uri->segments [4] ) );
		$pagesize = $this->setting ['list_default'];
		$startindex = ($page - 1) * $pagesize;
		$rownum = returnarraynum ( $this->db->query ( getwheresql ( 'tag_item', " tagid=" . $tag ['id'] . " and itemtype='article' ", $this->db->dbprefix ) )->row_array () );
		;
		
		// 获取tag动态列表
		$tagdoinglist = $this->tag_model->getlistbytagid ( $tag ['id'], $startindex, $pagesize, 'article' );
		
		if ($tagdoinglist) {
			// 获取相关标签
			$relativetags = $this->tag_model->gettaglistbycid ( $tagdoinglist [0] ['cid'] );
		}
		
		$departstr = page ( $rownum, $pagesize, $page, "tags/article/$tagalias" );
		$navtitle = $tag ['tagname'].'相关文章列表-'.$tag ['title'];
		$this->setting ['seo_index_description'] && $_seo_description = str_replace ( "{wzmc}", $this->setting ['site_name'], $this->setting ['seo_index_description'] );
		$this->setting ['seo_index_keywords'] && $_seo_keywords = str_replace ( "{wzmc}", $this->setting ['site_name'], $this->setting ['seo_index_keywords'] );
		
		$seo_description = $tag ['description']? $tag ['description']:$_seo_description;
		$seo_keywords = $tag ['keywords']? $tag ['keywords']:$_seo_keywords;
		include template ( 'tagview' );
	}
	function all(){
		$navtitle = '所有标签';
        //查找所有标签
		$page = max ( 1, intval ( $this->uri->segments [3] ) );
		$pagesize = 20;
		$startindex = ($page - 1) * $pagesize;
		$rownum = returnarraynum ( $this->db->query ( getwheresql ( 'tag', " 1=1 ", $this->db->dbprefix ) )->row_array () );
		// 获取tag动态列表
		$taglist = $this->tag_model->getalltaglist( $startindex, $pagesize );
		
		$departstr = page ( $rownum, $pagesize, $page, "tags/all" );
		
		include template ( 'tagall' );
	}
	/**
	
	* ajax获取标签
	
	* @date: 2018年11月7日 下午7:45:18
	
	* @author: 61703
	
	* @param: variable
	
	* @return:
	
	*/
	function ajaxsearch(){
		$tagname= htmlspecialchars ( $this->input->post('tagname'));
		//gettalistbytagname
		// 获取tag动态列表
		$taglist = $this->tag_model->gettalistbytagname($tagname, 0, 5 );
		$message['code']=200;
		$message['taglist']=$taglist;
		$message['tagname']=$tagname;
	
		echo json_encode($message);
		exit();
	}
}

?>