<?php

defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

class Admin_xiongzhang extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'topic_model' );
		$this->load->model ( 'question_model' );
	}
    function topiclist(){
    	$navtitle = "熊掌号文章推送列表";

    	@$page = max ( 1, intval ( $this->uri->segment ( 3 ) ) );
    	$pagesize = 50;
    	if($this->setting ['xiongzhang_tuisongnum']&&$this->setting ['xiongzhang_tuisongnum']!=null){
    		$pagesize = $this->setting ['xiongzhang_tuisongnum'];
    	}
    	$startindex = ($page - 1) * $pagesize;
    	$rownum = returnarraynum ( $this->db->query ( getwheresql ( 'topic', ' id>0 and state=1 ', $this->db->dbprefix ) )->row_array () );
    	$pages = @ceil ( $rownum / $pagesize );
    	$topiclist = $this->topic_model->get_list ( 2, $startindex, $pagesize );
  
    	$departstr = page ( $rownum, $pagesize, $page, "admin_xiongzhang/topiclist" );
    	include template ( "admin_xiongzhang_topic", "admin" );
    }
	function index($msg = '') {
		$msg && $message = $msg;
		//获取已审核的问题列表
		$navtitle = "熊掌号推送";
	
		//回答分页
		@$page = 1;
		
		@$page = max ( 1, intval ( $this->uri->rsegments [3]  ) );
		$pagesize = 50;
		if($this->setting ['xiongzhang_tuisongnum']&&$this->setting ['xiongzhang_tuisongnum']!=null){
			$pagesize = $this->setting ['xiongzhang_tuisongnum'];
		}
		$startindex = ($page - 1) * $pagesize;
		$paixu = 0; 
		
		
		$rownum = $this->question_model->rownum_by_cfield_cvalue_status ( '', 'all', 1, $paixu ); //获取总的记录数
		$questionlist = $this->question_model->list_by_cfield_cvalue_status ( '', 'all', 'all', $startindex, $pagesize, $paixu ); //问题列表数据
		
		$departstr = page ( $rownum, $pagesize, $page, "admin_xiongzhang/index" ); //得到分页字符串

		include template ( "admin_xiongzhang", "admin" );
	}
	function topicnewtui(){
		$urls = array ();
		
		if (null !== $this->input->post ( 'qid' )) {
			//SITE_URL.$suffix."q-$item[id]$fix
			$qids = $this->input->post ( 'qid' );
			$q_size = count ( $qids );
			for($i = 0; $i < $q_size; $i ++) {
				array_push ( $urls, url("topic/getone/".$qids [$i]) );
			}
		} else {
			$this->message( '您还没选择推送文章!' );
		}
		if (trim ( $this->setting ['xiongzhang_settingnewapi'] ) != '' && $this->setting ['xiongzhang_settingnewapi'] != null) {
			
			$api = $this->setting ['xiongzhang_settingnewapi'];
			
			$ch = curl_init();
			$options =  array(
					CURLOPT_URL => $api,
					CURLOPT_POST => true,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POSTFIELDS => implode("\n", $urls),
					CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
			);
			curl_setopt_array($ch, $options);
			$result =json_decode(curl_exec($ch),true) ;
			
			if(!$result['success_realtime']&&$result['success']==0){
				$this->message ( '文章推送不成功，请检查当前站点域名是否已经绑定熊掌号!' );;
			}
			if($result['success_realtime']&&$result['success_realtime']!=0){
				$this->message ( '文章推送成功!' );;
			}
		} else {
			$this->message ( '文章推送不成功，您还没设置熊掌号新增内容推送接口!' );
		}
	}
	function topichistorytui(){
		$urls = array ();
		
		if (null !== $this->input->post ( 'qid' )) {
			//SITE_URL.$suffix."q-$item[id]$fix
			$qids = $this->input->post ( 'qid' );
			$q_size = count ( $qids );
			for($i = 0; $i < $q_size; $i ++) {
				array_push ( $urls, url("topic/getone/".$qids [$i]) );
			}
		} else {
			$this->message ( '您还没选择推送文章!' );
		}
		if (trim ( $this->setting ['xiongzhang_settinghistoryapi'] ) != '' && $this->setting ['xiongzhang_settinghistoryapi'] != null) {
			
			$api = $this->setting ['xiongzhang_settinghistoryapi'];
			
			$ch = curl_init();
			$options =  array(
					CURLOPT_URL => $api,
					CURLOPT_POST => true,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POSTFIELDS => implode("\n", $urls),
					CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
			);
			curl_setopt_array($ch, $options);
			$result =json_decode(curl_exec($ch),true) ;
			
			if(!$result['success_batch']&&$result['success']==0){
				$this->message ( '文章推送不成功，请检查当前站点域名是否已经绑定熊掌号!' );;
			}
			if($result['success_batch']&&$result['success_batch']!=0){
				$this->message ( '文章推送成功!' );;
			}
			//$this->index ( '问题推送成功!' );
		} else {
			$this->message ( '文章推送不成功，您还没设置熊掌号历史内容推送接口!' );
		}
	}
	function newtui(){
		$urls = array ();
		
		if (null !== $this->input->post ( 'qid' )) {
			//SITE_URL.$suffix."q-$item[id]$fix
			$qids = $this->input->post ( 'qid' );
			$q_size = count ( $qids );
			for($i = 0; $i < $q_size; $i ++) {
				array_push ( $urls, url("question/view/".$qids [$i]) );
			}
		} else {
			$this->message( '您还没选择推送问题!' );
		}
		if (trim ( $this->setting ['xiongzhang_settingnewapi'] ) != '' && $this->setting ['xiongzhang_settingnewapi'] != null) {
			
			$api = $this->setting ['xiongzhang_settingnewapi'];

			$ch = curl_init();
			$options =  array(
					CURLOPT_URL => $api,
					CURLOPT_POST => true,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POSTFIELDS => implode("\n", $urls),
					CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
			);
			curl_setopt_array($ch, $options);
			$result =json_decode(curl_exec($ch),true) ;
		
			if(!$result['success_realtime']&&$result['success']==0){
				$this->message ( '问题推送不成功，请检查当前站点域名是否已经绑定熊掌号!' );;
			}
			if($result['success_realtime']&&$result['success_realtime']!=0){
				$this->message ( '问题推送成功!' );;
			}
		} else {
			$this->message ( '问题推送不成功，您还没设置熊掌号新增内容推送接口!' );
		}
	}
	
	function historytui(){
		$urls = array ();
		
		if (null !== $this->input->post ( 'qid' )) {
			//SITE_URL.$suffix."q-$item[id]$fix
			$qids = $this->input->post ( 'qid' );
			$q_size = count ( $qids );
			for($i = 0; $i < $q_size; $i ++) {
				array_push ( $urls, url("question/view/".$qids [$i]) );
			}
		} else {
			$this->message ( '您还没选择推送问题!' );
		}
		if (trim ( $this->setting ['xiongzhang_settinghistoryapi'] ) != '' && $this->setting ['xiongzhang_settinghistoryapi'] != null) {
			
			$api = $this->setting ['xiongzhang_settinghistoryapi'];
			
			$ch = curl_init();
			$options =  array(
					CURLOPT_URL => $api,
					CURLOPT_POST => true,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POSTFIELDS => implode("\n", $urls),
					CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
			);
			curl_setopt_array($ch, $options);
			$result =json_decode(curl_exec($ch),true) ;
			
			if(!$result['success_batch']&&$result['success']==0){
				$this->message ( '问题推送不成功，请检查当前站点域名是否已经绑定熊掌号!' );;
			}
			if($result['success_batch']&&$result['success_batch']!=0){
				$this->message ( '问题推送成功!' );;
			}
			//$this->index ( '问题推送成功!' );
		} else {
			$this->message ( '问题推送不成功，您还没设置熊掌号历史内容推送接口!' );
		}
	}
    function apiset(){
    	$this->load->model ( 'setting_model' );
    	if (null!== $this->input->post ('submit') ) {
    		if(!trim($this->input->post ('newcontent'))){
    			$this->message("新增内容接口不能为空");
    		}
    		if(!trim($this->input->post ('historycontent'))){
    			$this->message("历史内容接口不能为空");
    		}
    		$this->setting ['xiongzhang_settingnewapi'] = trim($this->input->post ('newcontent'));
    		$this->setting ['xiongzhang_settinghistoryapi'] = trim($this->input->post ('historycontent'));
    		$this->setting ['xiongzhang_tuisongnum'] = intval(trim($this->input->post ('xiongzhang_tuisongnum')));
    		if(	$this->setting ['xiongzhang_tuisongnum']>2000){
    			$this->message("推送内容数量最大一页2000条（熊掌号规定）。");
    		}
    		$this->setting_model->update ( $this->setting );
    		cleardir ( FCPATH . '/data/cache' ); //清除缓存文件
    		$this->index("配置成功");
    	}
    }
}

?>