<!--{template meta}-->
<section class="ui-container">

<div class="ws_header">
  <div class="ws_h_title">
  <div class="searchbar" onclick="window.location.href='{url question/searchkey}'">
    <i class="fa fa-search" ></i> <span>搜索感兴趣的内容</span>
  </div>
  </div>
  <i class="fa fa-pencil" ></i><i class="text">提问</i>
</div>
     <!--话题介绍-->
                    <div class="au_category_huati_info">
                           <div class="ui-row-flex">
                               <div class="ui-col">
                                   <div class="au_category_huati_img">
                                    <a href="{url category/view/$category['id']}">
                                       <img src="$category['bigimage']">
                                       </a>
                                   </div>
                               </div>
                               <div class="ui-col ui-col-3">
                                   <div class="au_category_huati_name"> <a  href="{url category/view/$category['id']}"> {$category['name']}</a></div>
                                   <div class="au_category_info_meta">
                                       <div class="au_category_info_meta_item"><i class="fa fa-question-circle hong"></i>{$category['questions']}个问题</div>
                                       <div class="au_category_info_meta_item"><i class="fa fa-user lan"></i>{$category['followers']}人关注</div>
                                       <div class="au_category_info_meta_item"><i class="fa fa-file-text-o ju "></i>{$trownum}篇文章</div>
                                   </div>
                               </div>
                         
                           </div>

                        <!--子话题-->
                            {if $sublist}
                        <div class="ui-row-flex">
                            <div class="ui-col ui-col au_category_info_childlist">

                                <div class="swiper-container" >
                                    <div class="swiper-wrapper">
                                      <!--{loop $sublist $index $cat}-->
                                        <div class="swiper-slide" data-swiper-autoplay="2000">
                                         
                                               
                                                    <div class="au_category_info_child">
                                                        <a href="{url category/view/$cat['id']}">
                                                        <div class="au_category_info_child_img">
                                                            <img src="$cat['image']">
                                                        </div>
                                                        <p class="au_category_info_child_text">{$cat['name']}</p>
                                                        </a>
                                                    </div>
                                               

                                            
                                        </div>
                                  <!--{/loop}--> 
                                    </div>

                                </div>
                                <!-- 如果需要导航按钮 -->
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>



                            </div>

                        </div>
{/if}
                    </div>
                              <!--导航提示-->
                    <div class="ws_cat_au_brif weui-flex">
                     <span class="weui-flex__item  <!--{if all==$status}-->current<!--{/if}-->"><a href="{url category/view/$cid/all}">全部问题</a> </span>
                       
                        <span class="weui-flex__item <!--{if 1==$status}-->current<!--{/if}-->"><a href="{url category/view/$cid/1}">
      未解决</a></span>
                        <span class="weui-flex__item <!--{if 2==$status}-->current<!--{/if}-->"><a  href="{url category/view/$cid/2}"> 已解决</a></span>
                        <span class="weui-flex__item <!--{if 6==$status}-->current<!--{/if}-->"> <a href="{url category/view/$cid/6}"> 推荐问题</a></span>
                       {if $category['isusearticle']}  <span class="weui-flex__item"><a href="{url topic/catlist/$cid}"> 相关文章</a></span> {/if}
                    </div>
                     <!--列表部分-->
                
                    <div class="qlists">

      <!--{loop $questionlist $index $question}-->
    <!--多图文-->
<div class="qlist">
<div class="title weui-flex">
    <div>
    {if $question['hidden']==1}
        <img src="{SITE_URL}static/css/default/avatar.gif">
        {else}
        <img src="{$question['avatar']}">
        {/if}
    </div>
    <div class="weui-flex__item">
       {if $question['hidden']==1}
         <span class="author">匿名用户 </span> 
       
          {else}
          <span class="author">{$question['author']}   {if $question['author_has_vertify']!=false}
        <i class="fa fa-vimeo {if $question['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  " data-toggle="tooltip" data-placement="right" title="" {if $question['author_has_vertify'][0]==0}data-original-title="个人认证" {else}data-original-title="企业认证" {/if} ></i>{/if}
         </span>
       
            {/if}
    </div>

</div>
    <p class="qtitle"><a href="{url question/view/$question['id']}">{$question['title']}</a></p>
   
   {if count($question['images'][1])>0}  
<div class="weui-flex">
 {if count($question['images'][1])>1}
 {loop  $question['images'][1] $index $img}
  {if  $index<=2}
    <div class="weui-flex__item"><div class="imgthumbsmall"> <a href="{url question/view/$question['id']}"><img src="{$img}"></a></div></div>
    {/if}
 {/loop}
 {else}
  {if $question['image']!=null&&count($question['images'][1])<=1}
  {loop  $question['images'][1] $index $img}
   <div class="weui-flex__item"><div class="imgthumbbig"><a href="{url question/view/$question['id']}"><img src="{$img}"></a></div></div>
   {/loop}
  {/if}
 {/if}
</div>
 {/if}
<p class="description">

 <a href="{url question/view/$question['id']}">{eval echo clearhtml($question['description']);} </a>
</p>
    <p class="meta">
       <span>
          <i class="icon_huida"></i>{$question['answers']}
       </span>
        <span>
           <i class="icon_liulan"></i> {$question['views']}
       </span>
    </p>
</div>
   
   
  <!--{/loop}-->
</div>
               <div class="pages">{$departstr}</div>
                    

<script>
    var swiper = new Swiper('.swiper-container', {
        loop:true,
        autoplay:2000,
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 10,
        // 如果需要前进后退按钮
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });
    var intswper=setInterval("swiper.slideNext()", 2000);
    $(".swiper-container").hover(function(){
        clearInterval(intswper);
    },function(){
        intswper=setInterval("swiper.slideNext()", 2000);
    })
</script>
</section>
<!--{template footer}-->