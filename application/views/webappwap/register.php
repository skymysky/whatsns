<!--{template meta}-->
    <style>
        body{
            background: #f1f5f8;
        }
        .au_login_panelform{
        margin-top:.53rem
        }
    </style>

 <div class="m_search">

    <div class="weui-flex">
        <div><i onclick="window.history.go(-1)" class="fa fa-angle-left"></i></div>
        <div class="weui-flex__item"> <span class="ws_h_title">用户注册</span></div>
        <div><span class="ws_ab_reg" onclick="window.location.href='{url user/login}'"><i class="fa fa-user-o"></i>登录</span></div>
    </div>
</div>
  <div class="au_login_panelform sign">
        <form class="new_user" method="post">
         <input type="hidden" id="tokenkey" name="tokenkey" value='{$_SESSION["registrtokenid"]}'/>
            <div class="input-prepend ">
                <input placeholder="你的昵称" type="text" value="" id="username" name="user_name" onblur="check_username();">
                <i class="fa fa-user"></i>
            </div>
            <div class="input-prepend ">
                <input placeholder="你的邮箱" type="text" value="" id="email" name="email" onblur="check_email();">
                <i class="fa fa-envelope"></i>
            </div>
              {if $setting['smscanuse']==1}
            <div class="input-prepend  no-radius js-normal ">

                <input placeholder="手机号" type="tel"  onblur="check_phone();" maxlength="11" id="userphone" name="userphone">
                <i class="fa fa-phone"></i>
            </div>

            <div class="input-prepend  no-radius security-up-code js-security-number ">
                <input type="text" id="seccode_verify" name="seccode_verify" placeholder="手机验证码" onblur="check_phone();">
                <i class="fa fa-get-pocket"></i>
                <a id="testbtn" onclick="gosms()" class="btn-up-resend js-send-code-button" href="javascript:;">发送验证码</a>

            </div>
             {else}

            <div class="input-prepend  no-radius js-normal ">
                    <img src="{url user/code}" onclick="javascript:updatecode();" id="verifycode" class="hide">

                    <input autocomplete="OFF" type="text" class="form-control" id="seccode_verify" name="seccode_verify" placeholder="验证码">
              <i class="fa fa fa-get-pocket"></i>
                  </div>
                {/if}
            <div class="input-prepend ">
                <input placeholder="设置密码" type="password" id="password" name="password" autocomplete="OFF" onblur="check_passwd();" maxlength="20">
                <i class="fa fa-lock"></i>
            </div>
            <div class="input-prepend">
                <input placeholder="确认密码" type="password" id="repassword" name="repassword" autocomplete="OFF"  onblur="check_repasswd();" maxlength="20">
                <i class="fa fa-lock"></i>
            </div>
  <div class="input-prepend ">
                <input placeholder="邀请码，非必填" type="text" {if $invatecode}readonly{/if} value="{if $invatecode}$invatecode{/if}" id="frominvatecode"  name="frominvatecode" >
                <i class="fa fa-envelope"></i>
            </div>
                       <div>
                <p>
                   <input type="checkbox" disabled id="agreeitem" value="1" checked/>                    <a id="showadreeitem">同意隐私条款[查看]</a>
                </p>
              
            </div>
            <input type="button" id="regsubmit" onclick="cheklogin()" value="注册" class="sign-up-button">
          
        </form>
            <!--{if $setting['sinalogin_open']||$wxbrower||$setting['qqlogin_open']}-->
        <!-- 更多登录方式 -->
        <div class="more-sign">

            <h6>第三方登录</h6>
            <ul>
                <!--{if $setting['sinalogin_open']}-->
                <li><a class="weibo" href="{SITE_URL}plugin/sinalogin/index.php"><i class="fa fa-weibo"></i></a></li>
                <!--{/if}-->
               <!--{if $wxbrower}-->
                <li><a class="weixin" href="{SITE_URL}?plugin_weixin/wxauth"><i class="fa fa-wechat"></i></a></li>
                <!--{/if}-->
                <!--{if $setting['qqlogin_open']}-->
                <li><a class="qq" href="{SITE_URL}plugin/qqlogin/index.php"><i class="fa fa-qq"></i></a></li>
                <!--{/if}-->



            </ul>

        </div>
         <!--{/if}-->
    </div>

    <div class="modal fade" id="agreemodel" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="padding-top: 50px;">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">网站注册协议说明</h4>
      </div>

        <div class="modal-body">
                    <div style="height: 450px;overflow:scroll;">
                        <p>&nbsp; &nbsp; &nbsp; &nbsp;当您注册时，表示您已经同意遵守本规章。&nbsp;</p><p>欢迎您加入本站点参加交流和讨论，本站点为公共论坛，为维护网上公共秩序和社会稳定，请您自觉遵守以下条款：&nbsp;</p><p><br></p><p>一、不得利用本站危害国家安全、泄露国家秘密，不得侵犯国家社会集体的和公民的合法权益，不得利用本站制作、复制和传播下列信息：</p><p>　 （一）煽动抗拒、破坏宪法和法律、行政法规实施的；</p><p>　（二）煽动颠覆国家政权，推翻社会主义制度的；</p><p>　（三）煽动分裂国家、破坏国家统一的；</p><p>　（四）煽动民族仇恨、民族歧视，破坏民族团结的；</p><p>　（五）捏造或者歪曲事实，散布谣言，扰乱社会秩序的；</p><p>　（六）宣扬封建迷信、淫秽、色情、赌博、暴力、凶杀、恐怖、教唆犯罪的；</p><p>　（七）公然侮辱他人或者捏造事实诽谤他人的，或者进行其他恶意攻击的；</p><p>　（八）损害国家机关信誉的；</p><p>　（九）其他违反宪法和法律行政法规的；</p><p>　（十）进行商业广告行为的。</p><p><br></p><p>二、互相尊重，对自己的言论和行为负责。</p><p>三、禁止在申请用户时使用相关本站的词汇，或是带有侮辱、毁谤、造谣类的或是有其含义的各种语言进行注册用户，否则我们会将其删除。</p><p>四、禁止以任何方式对本站进行各种破坏行为。</p><p>五、如果您有违反国家相关法律法规的行为，本站概不负责，您的登录论坛信息均被记录无疑，必要时，我们会向相关的国家管理部门提供此类信息。</p>
                    </div>
                </div>


    </div>
  </div>
</div>
    <section id="scripts">



<script type="text/javascript">
$(".close").click(function(){
	$('#agreemodel').hide();
});
$("#showadreeitem").click(function(){
	$('#agreemodel').show();
}
);
</script>


      <script type="text/javascript">
      setTimeout(function(){updatecode();$("#verifycode").removeClass("hide");},500);
    var usernameok = 1;
    var password = 1;
    var repasswdok = 1;
    var emailok = 1;
    var codeok = 1;
    function check_username() {
        var username = $.trim($('#username').val());
        var length = bytes(username);

        if (length < 3 || length > 15) {

        	el2=$.tips({
                content:'用户名请使用3到15个字符',
                stayTime:1000,
                type:"info"
            });


            usernameok = false;
        } else {
            $.post("{url user/ajaxusername}", {username: username}, function(flag) {
                if (-1 == flag) {


                	 el2=$.tips({
                         content:'此用户名已经存在',
                         stayTime:2000,
                         type:"info"
                     });

                    usernameok = false;
                } else if (-2 == flag) {


                	 el2=$.tips({
                         content:'用户名含有禁用字符',
                         stayTime:2000,
                         type:"info"
                     });
                    usernameok = false;
                } else {

                	 el2=$.tips({
                         content:'用户名可以使用',
                         stayTime:1000,
                         type:"success"
                     });

                    usernameok = true;
                }
            });
        }
    }

    function check_passwd() {
        var passwd = $('#password').val();
        if (bytes(passwd) < 6 || bytes(passwd) > 16) {




        	 el2=$.tips({
                 content:'密码最少6个字符，最长不得超过16个字符',
                 stayTime:2000,
                 type:"info"
             });
            password = false;
        } else {


            password = 1;
        }
    }

    function check_repasswd() {
        repasswdok = 1;
        var repassword = $('#repassword').val();
        if (bytes(repassword) < 6 || bytes(repassword) > 16) {
        	 el2=$.tips({
                 content:'密码最少6个字符，最长不得超过16个字符',
                 stayTime:2000,
                 type:"info"
             });

            repasswdok = false;
        } else {
            if ($('#password').val() == $('#repassword').val()) {


                repasswdok = true;
            } else {
            	 el2=$.tips({
                     content:'两次密码输入不一致',
                     stayTime:2000,
                     type:"info"
                 });

                repasswdok = false;
            }
        }
    }

    function check_email() {
        var email = $.trim($('#email').val());
        if (!email.match(/^[\w\.\-]+@([\w\-]+\.)+[a-z]{2,4}$/ig)) {



        	 el2=$.tips({
                 content:'邮件格式不正确',
                 stayTime:1000,
                 type:"info"
             });

            usernameok = false;
        } else {
            $.post("{url user/ajaxemail}", {email: email}, function(flag) {
                if (-1 == flag) {
                	 el2=$.tips({
                         content:'此邮件地址已经注册',
                         stayTime:1000,
                         type:"info"
                     });

                    emailok = false;
                } else if (-2 == flag) {
                	 el2=$.tips({
                         content:'邮件地址被禁止注册',
                         stayTime:1000,
                         type:"info"
                     });

                    emailok = false;
                } else {
                    emailok = true;

                	 el2=$.tips({
                         content:'邮箱名可以注册',
                         stayTime:1500,
                         type:"success"
                     });
                }
            });
        }
    }




    function cheklogin(){


        var _uname=$("#username").val();
        var _upwd=$("#password").val();
        var _rupwd=$("#repassword").val();
        var _code=$("#seccode_verify").val();
        var _email=$("#email").val();
        var _frominvatecode=$("#frominvatecode").val();
        var _apikey=$("#tokenkey").val();
        var el='';
        {if $setting['smscanuse']==1}
        var _phone=$("#userphone").val();

      	  var _rs=check_phone(_phone);
      	if(!_rs){
      		 alert("手机号码有误");
      		 return false;
      	}
        var _data={phone:_phone,uname:_uname,upwd:_upwd,rupwd:_rupwd,email:_email,apikey:_apikey,seccode_verify:_code};
        {else}
        var _data={uname:_uname,upwd:_upwd,rupwd:_rupwd,email:_email,frominvatecode:_frominvatecode,apikey:_apikey,seccode_verify:_code};
        {/if}
        $.ajax({
            //提交数据的类型 POST GET
            type:"POST",
            //提交的网址
            url:"{url api_user/registerapi}",
            //提交的数据
            data:_data,
            //返回数据的格式
            datatype: "text",//"xml", "html", "script", "json", "jsonp", "text".
            //在请求之前调用的函数
            beforeSend:function(){
         	    el=$.loading({
         	        content:'加载中...',
         	    })
            },
            //成功返回之后调用的函数
            success:function(data){
            	 el.loading("hide");

                if(data=='reguser_ok'){





                  window.location.href="{SITE_URL}?user/default";



                }else if(data=='reguser_ok1'){
                	 el2=$.tips({
                         content:'注册成功，系统已发送注册邮件，24小时之内请进行邮箱验证，在您没激活邮件之前你不能发布问题和文章等操作！',
                         stayTime:1500,
                         type:"success"
                     });

                	   window.location.href="{SITE_URL}?user/default";
                }else{
                	switch(data){


                	case 'reguser_cant_null':

                		 el2=$.tips({
                             content:'用户名或者密码不能为空',
                             stayTime:1000,
                             type:"info"
                         });

                		break;
                	case 'regemail_Illegal':
                		 el2=$.tips({
                             content:'注册邮箱不合法',
                             stayTime:1000,
                             type:"info"
                         });

                		break;
                	case 'regemail_has_exits':
                		 el2=$.tips({
                             content:'邮箱已注册',
                             stayTime:1000,
                             type:"info"
                         });

                		break;
                	case 'regemail_cant_use':
                		 el2=$.tips({
                             content:'此邮箱不能注册使用',
                             stayTime:1000,
                             type:"info"
                         });

                		break;
                	case 'reguser_has_exits':
                		 el2=$.tips({
                             content:'注册用户名已经存在',
                             stayTime:1000,
                             type:"info"
                         });

                		break;
                	case 'Illegal':
                		 el2=$.tips({
                             content:'用户名或者密码包含特殊字符',
                             stayTime:1000,
                             type:"info"
                         });

                		break;
                	default:

                		 el2=$.tips({
                             content:data,
                             stayTime:1000,
                             type:"info"
                         });
                		break;
                	}
                }
            }   ,
            //调用执行后调用的函数
            complete: function(XMLHttpRequest, textStatus){
         	    el.loading("hide");
            },
            //调用出错执行的函数
            error: function(){
                //请求出错处理
            }
        });
        return false;
    }



      //验证码
        function updatecode() {
            var img = g_site_url + "index.php" + query + "user/code/" + Math.random();
            $('#verifycode').attr("src", img);
        }

</script>
    </section>

<script src="{SITE_URL}static/css/fronze/js/main.js"></script>
