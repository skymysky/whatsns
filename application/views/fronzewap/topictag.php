<!--{template meta}-->
    <style>
        body{
            background: #f1f5f8;
        }
    </style>

    <div class="ws_header">
        <i class="fa fa-home" onclick="window.location.href='{url index}'"></i>
        <div class="ws_h_title">{$setting['site_name']}</div>
        <i class="fa fa-search"  onclick="window.location.href='{url question/searchkey}'"></i>
    </div>

    <!--导航提示-->
    <div class="ws_s_au_brif">
         <span class="ws_s_au_bref_item "><a href="{url question/search/$word}">问题</a></span>

        <span class="ws_s_au_bref_item current"><a href="{url topic/search}?word={$word}">文章</a></span>
        <span class="ws_s_au_bref_item"><a href="{url user/search}?word={$word}">用户</a></span>
        <span class="ws_s_au_bref_item"><a href="{url category/search}?word={$word}">话题</a></span>

    </div>
   <!--列表部分-->
                    <div class="au_resultitems au_searchlist">
                      <!--{if $topiclist}-->
  <div class="qlists">
        <div class="stream-list blog-stream">
     <!--{loop $topiclist $index $topic}-->   

<section class="stream-list__item"><div class="blog-rank stream__item"><div  class="stream__item-zan   btn btn-default mt0"><span class="stream__item-zan-icon"></span><span class="stream__item-zan-number">{$topic['articles']}</span></div></div><div class="summary"><h2 class="title blog-type-common blog-type-1"><a href="{url topic/getone/$topic['id']}">{$topic['title']}</a></h2><ul class="authorme list-inline"><li>
<span style="vertical-align:middle;"><a href="{url user/space/$topic['authorid']}">{$topic['author']}   {if $topic['author_has_vertify']!=false}
        <i class="fa fa-vimeo {if $topic['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  "  ></i>{/if}</a>
                    
                    发布于
                                           {$topic['format_time']}</span></li><li class="bookmark " title="{$topic['articles']} 收藏" ></li></ul></div></section>

  <!--{/loop}-->
</div>
</div>
        <div class="pages">  {$departstr}</div>
   <!--{else}-->
                            <div id="no-result">
                <p>抱歉，未找到和您搜索相关的内容。</p>
                <strong>建议您：</strong>
                <ul class="nav">
                    <li><span>检查输入是否正确</span></li>
                    <li><span>简化查询词或尝试其他相关词</span></li>
                </ul>
            </div>
    <!--{/if}-->

                    </div>
   <script>

   el2=$.tips({
        content:' 为您找到相关结果约{$rownum}个',
        stayTime:3000,
        type:"info"
    });
   </script>
<!--{template footer}-->