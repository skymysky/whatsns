<div class="user-header-info">
   <i class="ui-icon-set icon_setting"></i>
    <ul class="ui-row">
        <li class="ui-col ui-col-25">
            <div class="ui-avatar-lg">
                <span style="background-image:url({$user['avatar']})"></span>
            </div>
        </li>
        <li class="ui-col ui-col-75">
              <p>
     
                  <span class="ui-txt-highlight user-name">
                  {$user['username']}
                   {if $user['author_has_vertify']!=false}<i class="fa fa-vimeo {if $user['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  " data-toggle="tooltip" data-placement="right" title="" {if $user['author_has_vertify'][0]=='0'}data-original-title="个人认证" {else}data-original-title="企业认证" {/if} ></i>{/if}
                  </span>
              </p>

                <ul class="ui-user-tiled">
                    <li><div>{$user['answers']}</div><i>回答</i></li>
                    <li><div>{$user['questions']}</div><i>提问</i></li>
                    <li><div>{$user['followers']}</div><i>粉丝</i></li>
                    <li><div>{$user['credit1']}</div><i>经验</i></li>
                    <li><div>{$user['credit2']}</div><i>财富</i></li>
                
                </ul>

        </li>
     
        </ul>
        
        <section class="ui-leftsilde">
<div class="ui-actionsheet">  
  <div class="ui-actionsheet-cnt">
    <h4>我的管理中心导航</h4> 
      

    <button onclick="window.location.href='{url user/editimg}'">修改头像</button>  
     <button onclick="window.location.href='{url user/profile}'">修改个人信息</button>
     <button onclick="window.location.href='{url user/uppass}'">修改密码</button>
       <button onclick="window.location.href='{url user/editemail}'">激活邮箱和手机号</button>  
      <button class="ui-actionsheet-del" onclick="window.location.href='{url user/logout}'">
                退出
            </button>
    <button onclick="hidemenu()">取消</button> 
  </div>         
</div>

<script type="text/javascript">
$(".icon_setting").click(function(){
	 $('.ui-actionsheet').addClass('show');
})
function hidemenu(){
	 $('.ui-actionsheet').removeClass('show');
}
//编辑用户名和权限
function editmodle(){
	$("#mypay").val("{$user['mypay']}")
	 $('#dialogeditusername').dialog('show');
	
}
function change(){
	var _val=$("#mypay").val();
	if(parseInt(_val)<1){
		alert('最小金额不低于一元。')
		
		return false;
	}
	if(parseInt(_val)>20000){
	alert('最大金额不超过2W人民币。')
		
		return false;
	}
}
function editjine(){
	var _val=$("#mypay").val();
	if(parseInt(_val)<1){
		alert('最小金额不低于一元。')
		
		return false;
	}
	if(parseInt(_val)>20000){
		alert('最大金额不超过2W人民币。')
		
		return false;
	}

	   $.ajax({
		        //提交数据的类型 POST GET
		        type:"POST",
		        //提交的网址
		        url:"{SITE_URL}/?user/ajaxsetmypay",
		        //提交的数据
		        data:{mypay:_val},
		        //返回数据的格式
		        datatype: "text",//"xml", "html", "script", "json", "jsonp", "text".

		        //成功返回之后调用的函数
		        success:function(data){
		        
		          if(data=='1'){
		        	alert('设置成功!');
		        	setTimeout(function(){
		        		
		        		window.location.href=window.location.href;
		        	},1000)
		          }else{
		        	 alert('设置失败!')
		          }
		        	
		          
		        }   ,
		       
		        //调用出错执行的函数
		        error: function(){
		            //请求出错处理
		        }
		    });
}
</script>
    
</section>
</div>