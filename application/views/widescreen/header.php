<!--{template meta}-->
<div id="body">

    <header>
        <nav class="ws_header">
            <input type="hidden" id="lanage" value="zh">
            <div class="logo"><a href="{SITE_URL}" z-st="nav_logo"><img src="{$setting['site_logo']}" width="120" height="20" alt="{$setting['site_name']}"></a>

            </div>
            <div class="menu">
                <div class="search-input-hull" style="display:none">
                           <form name="searchform" action="{url question/search}"  method="post" accept-charset="UTF-8">
                    <span class="search-ipt"></span>
                    <input type="text"  autocomplete="off"  name="word" id="search-kw" value="" placeholder="{$setting['search_placeholder']}">
                    <span class="search-cancel"></span>
                     </form>
                  <div id="search-content" class="search-content">
                    <div class="search-content-list"><div class="search-title">热门搜索</div>
                                <!--{eval $hotwords = $this->fromcache("hotwords");}-->
                                      <!--{loop $hotwords $index $hotword}-->
                                      {if $index<8}
              <div class="hot-list search-l">
                    <a href="{url question/search}?word={$hotword['w']}" z-st="nav_searchbox_hotdesigner">{$hotword['w']}</a>
                    </div>
             {/if}
            
             <!--{/loop}-->
             
                 
                    
                    </div>
                </div>
                </div>
             

                <div class="menu-box">
                    <ul class="menu-list-content" >
                    
                      <!--{eval $headernavlist = $this->fromcache("headernavlist");}-->
                         <!--{loop $headernavlist $index $headernav}-->
                    <!--{if $headernav['type']==1 && $headernav['available']}-->

               {if $index<7}     
      <li class="<!--{if strstr($headernav['url'],$regular)}--> current<!--{/if}-->">
      <a class="menu-list-box" href="{$headernav['format_url']}" <!--{if $headernav['target']}-->target="_blank" <!--{/if}-->  title="{$headernav['title']}" >{$headernav['name']}</a></li>
                {/if}       
                    <!--{/if}-->
                    <!--{/loop}-->
                    
                  
              
                        <li class="more-menu header-menu-withmore ">
                            <span class="menu-tit-box" id="header-more" ><i></i></span>
                            <div class="menu-list hide">
                             
                                               <!--{loop $headernavlist $index $headernav}-->
                    <!--{if $headernav['type']==1 && $headernav['available']}-->

               {if $index>=7}     
         <p>
                         <a class="menu-list-box" href="{$headernav['format_url']}" <!--{if $headernav['target']}-->target="_blank" <!--{/if}-->  title="{$headernav['title']}" >{$headernav['name']}</a>
                                </p>
                {/if}       
                    <!--{/if}-->
                    <!--{/loop}-->
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="search" >
                <i></i>
            </div>
            <div class="user-center">
                <div class="upload">
                    <a href="javascript:;" class="upload-link" ></a>
                <div class="menu-list postlist hide">
                            <p>
                                <a href="{url question/add}" id="header-more-top" z-st="nav_tab_more_top">提问</a>
                            </p>
                            <p>
                                <a href="{url user/addxinzhi}" z-st="nav_tab_designer">发文</a>
                            </p>
                       
                        </div>
                </div>
                  <!--{if 0!=$user['uid']}-->
                <ul class="login">
                    <li class="message">
                        <a class="message-list"><i></i>
                        <sup class="subnav-dot-sup hide"></sup>
                        </a>
                        <div class="menu-list message-box hide">
                            <section>消息盒 </section>
                            <div id="contetn-2" class="message-box-list-wrapper">
                                <ul class="message-box-list mCustomScrollbar _mCS_1 mCS-autoHide mCS_no_scrollbar"><div id="mCSB_1" class="mCustomScrollBox mCS-dark mCSB_vertical mCSB_inside" style="max-height: 225px;" ><div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                                    <li class="news-circle personmsgbox"> </li>
                                <li class="news-circle systemmsgbox"> </li>
                                    </div>
                                    <div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-dark mCSB_scrollTools_vertical" style="display: none;"><a href="#" class="mCSB_buttonUp"></a><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; top: 0px;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div><a href="#" class="mCSB_buttonDown"></a></div></div></ul>
          
                            </div>
                                              <div id="empty_message_box" class="msg-box-null">
		<div class="msg-box-inner">
			<span class="null-images"></span>
			<span class="msg-null-tips">没有新消息</span>
		</div>
	</div>
                            <a  class="check-all" href="{url message/personal}">查看全部 </a>
                        </div>
                    </li>
                    <li class="user"><div class="avatar-container-30">
                        <a href="{url user/default}" class="user-list" ><img src="{$user['avatar']}" title="{$user['username']}" alt="{$user['username']}"></a>

                    </div>
                        <div class="menu-list user-box hide">
                            <section><a href="{url user/default}" z-st="nav_userbox_name">{$user['username']}</a></section>
                            <div class="user-box-list">
                                <div class="user-box-list-area">
                                  <!--{if $user['groupid']<=3}-->
                                  <p class="works-manange"><a href="{SITE_URL}index.php?admin_main" >后台管理</a></p>
                                  
                                    <!--{/if}-->
                                    <p class="works-manange"><a href="{url user/recommend}" >为我推荐</a></p>
                                    <p class="works-manange"><a href="{url user/ask}" >我的提问</a></p>
                                    <p class="works-manange"><a href="{url user/answer}" >我的回答</a></p>
                                    <p class="works-manange"><a href="{url topic/userxinzhi/$user['uid']}">我的文章</a></p>
                                    <p class="works-manange"><a href="{url user/attention/question}" >我的关注</a></p>
       
                                </div>
                                <div class="user-box-list-area">
                             
                                    <p class="works-manange"><a href="{url user/profile}" >资料与帐号</a></p>
                                </div>
                                <div class="user-box-list-area">
                                    <p class="works-manange"><a href="{url user/logout}" >退出</a></p>
                                </div>
                            </div>
                        </div></li>
                   
                </ul>
                   <!--{else}-->
                <ul class="unlogin ">
                    <li><a href="javascript:login();"  class="nav-unlogin">登录<i></i></a><a href="{url user/register}">注册</a></li>
                </ul>
                <!--{/if}-->
            </div>
        </nav>
       
    </header>
<script type="text/javascript">
$(".search-ipt").click(function(){
	var _txt=$.trim($("#search-kw").val());
	if(_txt==''){
		alert("关键词不能为空");
		return ;
	}
	 document.searchform.action = "{url question/search}";
    document.searchform.submit();
});
$(".ws_header .search,.ws_header .search-cancel").click(function(){
	$(".ws_header .search").toggle();
	$(".ws_header .menu-list-content").toggle();
	$(".ws_header .search-input-hull").toggle();
})
$(".message .message-list,.message .message-box").hover(function(){
	$(".message .message-box").show();
},function(){	$(".message .message-box").hide();});
$(".upload-link,.postlist").hover(function(){
	$(".postlist").show();
},function(){
	$(".postlist").hide();
})
</script>

    <div class="main-wrapper index">